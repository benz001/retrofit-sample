// To parse this JSON data, do
//
//     final UpdateProductResponse = UpdateProductResponseFromJson(jsonString);

import 'dart:convert';

UpdateProductResponse updateProductResponseFromJson(String str) => UpdateProductResponse.fromJson(json.decode(str));

String updateProductResponseToJson(UpdateProductResponse data) => json.encode(data.toJson());

class UpdateProductResponse {
    int id;
    String? title;
    String? description;
    int? price;
    double? discountPercentage;
    double? rating;
    int? stock;
    String? brand;
    String? category;
    String? thumbnail;
    List<String>? images;

    UpdateProductResponse({
        required this.id,
         this.title,
         this.description,
         this.price,
         this.discountPercentage,
         this.rating,
         this.stock,
         this.brand,
         this.category,
         this.thumbnail,
         this.images,
    });

    factory UpdateProductResponse.fromJson(Map<String, dynamic> json) => UpdateProductResponse(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        price: json["price"],
        discountPercentage: json["discountPercentage"]?.toDouble(),
        rating: json["rating"]?.toDouble(),
        stock: json["stock"],
        brand: json["brand"],
        category: json["category"],
        thumbnail: json["thumbnail"],
        images: List<String>.from(json["images"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "price": price,
        "discountPercentage": discountPercentage,
        "rating": rating,
        "stock": stock,
        "brand": brand,
        "category": category,
        "thumbnail": thumbnail,
        "images": List<dynamic>.from(images?.map((x) => x)??[]),
    };
}
