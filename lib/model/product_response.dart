// To parse this JSON data, do
//
//     final DataProductResponse = DataProductResponseFromJson(jsonString);

import 'dart:convert';

ProductResponse productResponseFromJson(String str) => ProductResponse.fromJson(json.decode(str));

String productResponseToJson(ProductResponse data) => json.encode(data.toJson());

List<DataProduct> listProductFromJson(String str) => List<DataProduct>.from(json.decode(str).map((x) => DataProduct.fromJson(x)));

String listProductToJson(List<DataProduct> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));


class ProductResponse {
    List<DataProduct> DataProducts;
    int total;
    int skip;
    int limit;

    ProductResponse({
        required this.DataProducts,
        required this.total,
        required this.skip,
        required this.limit,
    });

    factory ProductResponse.fromJson(Map<String, dynamic> json) => ProductResponse(
        DataProducts: List<DataProduct>.from(json["products"].map((x) => DataProduct.fromJson(x))),
        total: json["total"],
        skip: json["skip"],
        limit: json["limit"],
    );

    Map<String, dynamic> toJson() => {
        "products": List<dynamic>.from(DataProducts.map((x) => x.toJson())),
        "total": total,
        "skip": skip,
        "limit": limit,
    };
}

class DataProduct {
    int id;
    String title;
    String description;
    int price;
    double discountPercentage;
    double rating;
    int stock;
    String brand;
    String category;
    String thumbnail;
    List<String> images;

    DataProduct({
        required this.id,
        required this.title,
        required this.description,
        required this.price,
        required this.discountPercentage,
        required this.rating,
        required this.stock,
        required this.brand,
        required this.category,
        required this.thumbnail,
        required this.images,
    });

    factory DataProduct.fromJson(Map<String, dynamic> json) => DataProduct(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        price: json["price"],
        discountPercentage: json["discountPercentage"]?.toDouble(),
        rating: json["rating"]?.toDouble(),
        stock: json["stock"],
        brand: json["brand"],
        category: json["category"],
        thumbnail: json["thumbnail"],
        images: List<String>.from(json["images"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "price": price,
        "discountPercentage": discountPercentage,
        "rating": rating,
        "stock": stock,
        "brand": brand,
        "category": category,
        "thumbnail": thumbnail,
        "images": List<dynamic>.from(images.map((x) => x)),
    };
}
