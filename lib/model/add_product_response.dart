// To parse this JSON data, do
//
//     final AddProductReponse = AddProductReponseFromJson(jsonString);

import 'dart:convert';

AddProductResponse AddProductReponseFromJson(String str) => AddProductResponse.fromJson(json.decode(str));

String AddProductReponseToJson(AddProductResponse data) => json.encode(data.toJson());

class AddProductResponse {
    int id;
    String title;

    AddProductResponse({
        required this.id,
        required this.title,
    });

    factory AddProductResponse.fromJson(Map<String, dynamic> json) => AddProductResponse(
        id: json["id"],
        title: json["title"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
    };
}
