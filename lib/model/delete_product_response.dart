// To parse this JSON data, do
//
//     final DeleteProductResponse = DeleteProductResponseFromJson(jsonString);

import 'dart:convert';

DeleteProductResponse DeleteProductResponseFromJson(String str) => DeleteProductResponse.fromJson(json.decode(str));

String DeleteProductResponseToJson(DeleteProductResponse data) => json.encode(data.toJson());

class DeleteProductResponse {
    int id;
    String? title;
    String? description;
    int? price;
    double? discountPercentage;
    double? rating;
    int? stock;
    String? brand;
    String? category;
    String? thumbnail;
    List<String>? images;
    bool? isDeleted;
    DateTime? deletedOn;

    DeleteProductResponse({
        required this.id,
         this.title,
         this.description,
         this.price,
         this.discountPercentage,
         this.rating,
         this.stock,
         this.brand,
         this.category,
         this.thumbnail,
         this.images,
         this.isDeleted,
         this.deletedOn,
    });

    factory DeleteProductResponse.fromJson(Map<String, dynamic> json) => DeleteProductResponse(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        price: json["price"],
        discountPercentage: json["discountPercentage"]?.toDouble(),
        rating: json["rating"]?.toDouble(),
        stock: json["stock"],
        brand: json["brand"],
        category: json["category"],
        thumbnail: json["thumbnail"],
        images: List<String>.from(json["images"].map((x) => x)),
        isDeleted: json["isDeleted"],
        deletedOn:json["deletedOn"] != null? DateTime.parse(json["deletedOn"]):null,
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "price": price,
        "discountPercentage": discountPercentage,
        "rating": rating,
        "stock": stock,
        "brand": brand,
        "category": category,
        "thumbnail": thumbnail,
        "images": List<dynamic>.from(images?.map((x) => x)??[]),
        "isDeleted": isDeleted,
        "deletedOn": deletedOn
    };
}
