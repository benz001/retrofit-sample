import 'package:flutter/material.dart';

class StateHandleList extends StatelessWidget {
  final bool isLoading;
  final bool isSuccess;
  final Widget widgetSuccess;
  final Widget? widgetEmpty;
  final List<dynamic> data;
  final Widget? widgetError;
  final Function()? onPressedWidgetError;
  const StateHandleList(
      {super.key,
      required this.isLoading,
      required this.isSuccess,
      required this.widgetSuccess,
      required this.data,
      this.widgetEmpty,
      this.widgetError,
      this.onPressedWidgetError});

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      if (isLoading) {
        return const Center(
          child: CircularProgressIndicator(
            color: Colors.blue,
          ),
        );
      } else {
        if (isSuccess) {
          return data.isNotEmpty
              ? widgetSuccess
              : widgetEmpty ??
                  const Center(
                    child: Text('Data is Empty'),
                  );
        } else {
          return widgetError ??
              Center(
                child: ElevatedButton(
                    onPressed: onPressedWidgetError,
                    child: const Text('Reload')),
              );
        }
      }
    });
  }
}
