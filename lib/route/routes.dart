import 'package:get/get.dart';
import 'package:retrofit_sample/binding/product_binding.dart';
import 'package:retrofit_sample/binding/todo_binding.dart';
import 'package:retrofit_sample/ui/list_product_page.dart';
import 'package:retrofit_sample/ui/list_todo.dart';

class Routes {
  static List<GetPage<dynamic>>? getPages() {
    return [
      GetPage(
          name: ListProductPage.routeName,
          page: () => ListProductPage(),
          binding: ProductBinding()),
       GetPage(
          name: ListToDoPage.routeName,
          page: () => ListToDoPage(),
          binding: TodoBinding())
    ];
  }
}