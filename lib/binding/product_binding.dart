import 'package:get/get.dart';
import 'package:retrofit_sample/controller/product_controller.dart';

class ProductBinding extends Bindings{
  @override
  void dependencies() {
   Get.lazyPut(() => ProductController());
  }
  
}