import 'package:get/get.dart';
import 'package:retrofit_sample/controller/to_do_controller.dart';

class TodoBinding extends Bindings  {
  @override
  void dependencies() {
   Get.lazyPut(() => ToDoController());
  }
  
}