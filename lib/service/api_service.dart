import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:retrofit_sample/helper/logging_interceptor.dart';
import 'package:retrofit_sample/helper/url_helper.dart';
import 'package:retrofit_sample/model/add_product_response.dart';
import 'package:retrofit_sample/model/delete_product_response.dart';
import 'package:retrofit_sample/model/product_response.dart';
import 'package:retrofit_sample/model/update_product_response.dart';

part 'api_service.g.dart';

@RestApi()
abstract class APIService {
  factory APIService(Dio dio) =
      _APIService;

  static Future<APIService> createService() async {
    final dio = Dio();
    dio.interceptors.add(LoggingInterceptor());
    return APIService(dio);
  }

  @GET("${URLHelper.urlSample}/products")
  Future<ProductResponse> getListProduct();

  @POST("${URLHelper.urlSample}/products/add")
  Future<AddProductResponse> postAddProduct(
      @Field('title') String productName);

  @PUT("${URLHelper.urlSample}/products/{id}")
  Future<UpdateProductResponse> putUpdateProduct(
      @Path('id') int id, @Field('title') String productName);

  @DELETE("${URLHelper.urlSample}/products/{id}")
  Future<DeleteProductResponse> deleteRemoveProduct(
      @Path('id') int id);
}

final apiServiceProduct = APIService.createService();