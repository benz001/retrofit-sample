import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart' hide Headers;
import 'package:retrofit_sample/helper/logging_interceptor.dart';
import 'package:retrofit_sample/model/todo_response.dart';

part 'api_service_todo.g.dart';

@RestApi()
abstract class APIServiceTodo {
  factory APIServiceTodo(Dio dio) =
      _APIServiceTodo;

  static Future<APIServiceTodo> createService() async {
    final dio = Dio();
    dio.interceptors.add(LoggingInterceptor());
    return APIServiceTodo(dio);
  }

  @GET("https://dummyjson.com/todos")
  Future<TodoResponse> getListToDo({@Header('Accept')String? accept, @Header('Authorization')String? authorization});
}

final apiServiceProduct = APIServiceTodo.createService();