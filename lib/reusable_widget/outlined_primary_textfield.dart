import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class OutlinedPrimaryTextfield extends StatelessWidget {
  final String? initialValue;
  final TextEditingController? controller;
  final String? Function(String?)? validator;
  final TextInputType? keyboardType;
  final TextAlign? textAlign;
  final List<TextInputFormatter>? inputFormatters;
  final String? labelText;
  final TextStyle? labelStyle;
  final String? errorText;
  final String? hintText;
  final TextStyle? hintStyle;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final Color? fillColor;
  final BorderRadius? borderRadius;
  final BorderSide? borderSide;
  final double? gapPadding;
  final int? minLines;
  final int? maxLines;
  final bool? obscureText;
  final Function(String)? onChanged;
  final Function()? onEditingComplete;
  final Function(String)? onFieldSubmitted;
  final Function()? onTap;
  final bool? readOnly;
  final InputBorder? errorBorder;
  const OutlinedPrimaryTextfield(
      {super.key,
      this.initialValue,
      this.controller,
      this.validator,
      this.keyboardType,
      this.textAlign,
      this.inputFormatters,
      this.minLines,
      this.maxLines,
      this.labelText,
      this.labelStyle,
      this.obscureText,
      this.onChanged,
      this.onEditingComplete,
      this.onFieldSubmitted,
      this.onTap,
      this.errorText,
      this.hintText,
      this.hintStyle,
      this.suffixIcon,
      this.prefixIcon,
      this.fillColor,
      this.borderSide,
      this.borderRadius,
      this.gapPadding,
      this.readOnly,
      this.errorBorder});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: initialValue,
      controller: controller,
      validator: validator,
      keyboardType: keyboardType,
      textAlign: textAlign ?? TextAlign.start,
      inputFormatters: inputFormatters,
      minLines: minLines,
      maxLines: maxLines,
      obscureText: obscureText ?? false,
      onChanged: onChanged,
      onEditingComplete: onEditingComplete,
      onFieldSubmitted: onFieldSubmitted,
      onTap: onTap,
      readOnly: readOnly??false,
      decoration: InputDecoration(
          labelText: labelText,
          labelStyle: labelStyle,
          errorText: errorText,
          hintText: hintText,
          hintStyle: hintStyle,
          border: OutlineInputBorder(borderRadius: borderRadius??BorderRadius.only(topLeft: Radius.circular(4), topRight: Radius.circular(4)), borderSide: borderSide?? BorderSide(), gapPadding: gapPadding??4.0),
          suffixIcon: suffixIcon,
          prefixIcon: prefixIcon,
          filled: true,
          fillColor: fillColor,
          errorBorder: errorBorder),
    );
  }
}
  