
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:retrofit_sample/controller/product_controller.dart';
import 'package:retrofit_sample/helper/state_handle_list.dart';
import 'package:retrofit_sample/ui/widget/bottom_sheet_product.dart';
import 'package:retrofit_sample/ui/widget/list_product_builder.dart';

class ListProductPage extends StatelessWidget {
  static const routeName = '/list-product';
  ListProductPage({super.key});
  final ProductController _productController = Get.find<ProductController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('List Product'),
      ),
      body: GetBuilder<ProductController>(builder: (controller) {
        return Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.grey[300],
            child: StateHandleList(
                isLoading: controller.isLoadingProductList,
                isSuccess: controller.isSuccessProductList,
                data: controller.listDataProduct,
                onPressedWidgetError: () => controller.getListProduct(),
                widgetSuccess: ListProductBuilder(
                    context: context,
                    controller: controller,
                    list: controller.listDataProduct)));
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _productController.clearVariable();
          BottomSheetProduct bottomSheetProduct = BottomSheetProduct(
              context: context, controller: _productController);
          bottomSheetProduct.showAdd();
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
