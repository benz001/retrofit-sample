import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:retrofit_sample/controller/to_do_controller.dart';
import 'package:retrofit_sample/helper/state_handle_list.dart';
import 'package:retrofit_sample/ui/widget/list_todo_builder.dart';

class ListToDoPage extends StatelessWidget {
  static const routeName = '/list-todo-page';
  ListToDoPage({super.key});
  final ToDoController _toDoController = Get.find<ToDoController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('List Todo'),
      ),
      body: GetBuilder<ToDoController>(builder: (controller) {
        return Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.grey[300],
            child: StateHandleList(
                isLoading: controller.isLoadingListTodo,
                isSuccess: controller.isSuccessListTodo,
                data: controller.listTodo,
                onPressedWidgetError: () => controller.getListTodo(),
                widgetSuccess: ListToDoBuilder(
                    context: context,
                    controller: controller,
                    list: controller.listTodo)));
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {

        },
        child: const Icon(Icons.add),
      ),
    );
  }
}