import 'package:flutter/material.dart';
import 'package:retrofit_sample/controller/product_controller.dart';
import 'package:retrofit_sample/model/product_response.dart';
import 'package:retrofit_sample/ui/widget/bottom_sheet_product.dart';

class ItemProductBuilder extends StatelessWidget {
  final ProductController controller;
  final DataProduct item;
  const ItemProductBuilder(
      {super.key, required this.controller, required this.item});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(10),
      decoration: const BoxDecoration(
          color: Colors.white,
          border: Border(bottom: BorderSide(color: Colors.grey, width: 1))),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                width: 50,
                height: 50,
                color: Colors.transparent,
                child: Image.network(item.images[0]),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: Container(
                  color: Colors.transparent,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(item.title),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(item.price.toString())
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            width: double.infinity,
            color: Colors.transparent,
            alignment: Alignment.bottomRight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  icon: const Icon(
                    Icons.edit,
                    color: Colors.green,
                  ),
                  onPressed: () {
                    controller.clearVariable();
                    controller.productId = item.id;
                    controller.productNameController.text = item.title;
                    BottomSheetProduct bottomSheetProduct = BottomSheetProduct(
                        context: context, controller: controller);
                    bottomSheetProduct.showUpdate();
                  },
                ),
                const SizedBox(
                  width: 5,
                ),
                IconButton(
                  icon: const Icon(
                    Icons.delete,
                    color: Colors.red,
                  ),
                  onPressed: () {
                    controller.clearVariable();
                    controller.productId = item.id;
                    BottomSheetProduct bottomSheetProduct = BottomSheetProduct(
                        context: context, controller: controller);
                    bottomSheetProduct.showDelete();
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}