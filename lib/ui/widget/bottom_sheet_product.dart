import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:retrofit_sample/controller/product_controller.dart';
import 'package:retrofit_sample/reusable_widget/outlined_primary_textfield.dart';

class BottomSheetProduct extends StatelessWidget {
  final BuildContext context;
  final ProductController controller;

  const BottomSheetProduct(
      {super.key, required this.context, required this.controller});

  void showAdd() {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.25,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      color: Colors.transparent,
                      margin: const EdgeInsets.only(top: 10),
                      child: Container(
                        color: Colors.transparent,
                        child: OutlinedPrimaryTextfield(
                          controller: controller.productNameController,
                          hintText: 'Enter Product Name',
                        ),
                      ))),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      width: double.infinity,
                      height: 45,
                      margin: const EdgeInsets.only(bottom: 10),
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          child: ElevatedButton(
                            onPressed: () {
                              if (controller
                                  .productNameController.text.isNotEmpty) {
                                  controller.addProduct().then((value) {
                                    if (value) {
                                      Get.back();
                                      controller.getListProduct();
                                    }
                                  });
                              } else {
                                Fluttertoast.showToast(
                                    msg: 'Field is required');
                              }
                            },
                            child: const Text('ADD'),
                          ))))
            ],
          ),
        ));
  }

  void showUpdate() {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.25,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      color: Colors.transparent,
                      margin: const EdgeInsets.only(top: 10),
                      child: Container(
                        color: Colors.transparent,
                        child: OutlinedPrimaryTextfield(
                          controller: controller.productNameController,
                          hintText: 'Enter Product Name',
                        ),
                      ))),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      width: double.infinity,
                      height: 45,
                      margin: const EdgeInsets.only(bottom: 10),
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          child: ElevatedButton(
                            onPressed: () {
                              if (controller
                                  .productNameController.text.isNotEmpty) {
                                controller.updateProduct().then((value) {
                                  if (value) {
                                    Get.back();
                                    controller.getListProduct();
                                  }
                                });
                              } else {
                                Fluttertoast.showToast(
                                    msg: 'Field is required');
                              }
                            },
                            child: const Text('UPDATE'),
                          ))))
            ],
          ),
        ));
  }

  showDelete() {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.25,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          margin: const EdgeInsets.only(top: 10),
                          child: const Text(
                              'Are You sure to delete this product ?')))),
              Align(
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                        backgroundColor: Colors.red),
                                    onPressed: () {
                                      controller.deleteProduct().then((value) {
                                        if (value) {
                                          Get.back();
                                          controller.getListProduct();
                                        }
                                      });
                                    },
                                    child: const Text('Yes')))),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                        backgroundColor: Colors.blue),
                                    onPressed: () {
                                      Get.back();
                                    },
                                    child: const Text('No')))),
                      ),
                    ],
                  ))
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
