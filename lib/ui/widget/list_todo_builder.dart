import 'package:flutter/material.dart';
import 'package:retrofit_sample/controller/product_controller.dart';
import 'package:retrofit_sample/controller/to_do_controller.dart';
import 'package:retrofit_sample/model/product_response.dart';
import 'package:retrofit_sample/model/todo_response.dart';
import 'package:retrofit_sample/ui/widget/item_product_builder.dart';
import 'package:retrofit_sample/ui/widget/item_todo_builder.dart';

class ListToDoBuilder extends StatelessWidget {
  final BuildContext context;
  final ToDoController controller;
  final List<Todo> list;
  const ListToDoBuilder(
      {super.key,
      required this.context,
      required this.controller,
      required this.list});

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        controller.getListTodo();
      },
      child: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            return ItemTodoBuilder(
                controller: controller, item: list[index]);
          }),
    );
  }
}