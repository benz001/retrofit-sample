import 'package:flutter/material.dart';
import 'package:retrofit_sample/controller/product_controller.dart';
import 'package:retrofit_sample/controller/to_do_controller.dart';
import 'package:retrofit_sample/model/product_response.dart';
import 'package:retrofit_sample/model/todo_response.dart';
import 'package:retrofit_sample/ui/widget/bottom_sheet_product.dart';

class ItemTodoBuilder extends StatelessWidget {
  final ToDoController controller;
  final Todo item;
  const ItemTodoBuilder(
      {super.key, required this.controller, required this.item});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(10),
      decoration: const BoxDecoration(
          color: Colors.white,
          border: Border(bottom: BorderSide(color: Colors.grey, width: 1))),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  color: Colors.transparent,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(item.todo),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(item.completed? 'Done':'Progress')
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            width: double.infinity,
            color: Colors.transparent,
            alignment: Alignment.bottomRight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  icon: const Icon(
                    Icons.edit,
                    color: Colors.green,
                  ),
                  onPressed: () {
     
                  },
                ),
                const SizedBox(
                  width: 5,
                ),
                IconButton(
                  icon: const Icon(
                    Icons.delete,
                    color: Colors.red,
                  ),
                  onPressed: () {
 
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}