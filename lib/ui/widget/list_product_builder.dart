import 'package:flutter/material.dart';
import 'package:retrofit_sample/controller/product_controller.dart';
import 'package:retrofit_sample/model/product_response.dart';
import 'package:retrofit_sample/ui/widget/item_product_builder.dart';

class ListProductBuilder extends StatelessWidget {
  final BuildContext context;
  final ProductController controller;
  final List<DataProduct> list;
  const ListProductBuilder(
      {super.key,
      required this.context,
      required this.controller,
      required this.list});

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        controller.getListProduct();
      },
      child: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            return ItemProductBuilder(
                controller: controller, item: list[index]);
          }),
    );
  }
}