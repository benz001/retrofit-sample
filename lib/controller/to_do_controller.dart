import 'package:get/get.dart';
import 'package:retrofit_sample/model/todo_response.dart';
import 'package:retrofit_sample/service/api_service_todo.dart';

class ToDoController extends GetxController {
  //Alternatif bisa pakai enum
  bool isLoadingListTodo = false;
  bool isSuccessListTodo = false;
  List<Todo> listTodo = [];

  @override
  void onInit() {
    getListTodo();
    super.onInit();
  }

  void getListTodo() async {
    isLoadingListTodo = true;
    isSuccessListTodo = false;
    try {
      final service = await APIServiceTodo.createService();
      final result = await service.getListToDo('application/json');
      if (result.todos.isNotEmpty) {
        isLoadingListTodo = false;
        isSuccessListTodo = true;
        listTodo = result.todos;
        update();
      } else {
        isLoadingListTodo = false;
        isSuccessListTodo = false;
        listTodo = result.todos;
        update();
      }
    } catch (e) {
      isLoadingListTodo = false;
      isSuccessListTodo = false;
      update();
    }
  }
}
