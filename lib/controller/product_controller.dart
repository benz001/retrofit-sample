

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:retrofit_sample/model/product_response.dart';
import 'package:retrofit_sample/service/api_service.dart';

class ProductController extends GetxController {
  bool isLoadingProductList = false;
  bool isSuccessProductList = false;
  List<DataProduct> listDataProduct = [];

  RxBool isLoadingAddProduct = false.obs;
  RxBool isSuccessAddProduct = false.obs;
  final TextEditingController productNameController =
      TextEditingController(text: '');

  RxBool isLoadingUpdateProduct = false.obs;
  RxBool isSuccessUpdateProduct = false.obs;
  int productId = 0;

  RxBool isLoadingDeleteProduct = false.obs;
  RxBool isSuccessDeleteProduct = false.obs;





  @override
  void onInit() {
    // getListProduct();
    getListProduct();

    // MobileNumber.listenPhonePermission((isPermissionGranted) {
    //   if (isPermissionGranted) {
    //     initMobileNumberState();
    //   }
    // });

    super.onInit();
  }


  void getListProduct() async {
    isLoadingProductList = true;
    isSuccessProductList = false;
    listDataProduct.clear();
    update();
    final service = await APIService.createService();
    final result = await service.getListProduct();
    if (result.DataProducts.isEmpty) {
      isLoadingProductList = false;
      isSuccessProductList = false;
      listDataProduct = [];
      update();
    } else {
      isLoadingProductList = false;
      isSuccessProductList = true;
      listDataProduct = result.DataProducts;
      update();
    }
  }

  Future<bool> addProduct() async {
    isLoadingAddProduct.value = true;
    isSuccessAddProduct.value = false;
    update();
    final service = await APIService.createService();
    final result = await service.postAddProduct(productNameController.text);
    if (result.id != 0) {
      isLoadingAddProduct.value = false;
      isSuccessAddProduct.value = true;
      Fluttertoast.showToast(msg: 'Add Product Success');
      return true;
    } else {
      isLoadingAddProduct.value = false;
      isSuccessAddProduct.value = false;
      Fluttertoast.showToast(msg: 'Not Connection');
      return false;
    }
  }

  Future<bool> updateProduct() async {
    isLoadingUpdateProduct.value = true;
    isSuccessUpdateProduct.value = false;
    final service = await APIService.createService();
    final result =
        await service.putUpdateProduct(productId, productNameController.text);
    if (result.id != 0) {
      isLoadingUpdateProduct.value = false;
      isSuccessUpdateProduct.value = true;
      Fluttertoast.showToast(msg: 'Update Product Success');
      return true;
    } else {
      isLoadingUpdateProduct.value = false;
      isSuccessUpdateProduct.value = false;
      Fluttertoast.showToast(msg: 'Update Product Failed');
      return false;
    }
  }

  Future<bool> deleteProduct() async {
    isLoadingDeleteProduct.value = true;
    isSuccessDeleteProduct.value = false;
    final service = await APIService.createService();
    final result =
        await service.deleteRemoveProduct(productId);
    if (result.id != 0) {
      isLoadingDeleteProduct.value = false;
      isSuccessDeleteProduct.value = true;
      Fluttertoast.showToast(msg: 'Delete Product Success');
      return true;
    } else {
      isLoadingDeleteProduct.value = false;
      isSuccessDeleteProduct.value = false;
      Fluttertoast.showToast(msg: 'Delete Product Failed');
      return false;
    }
  }

  void clearVariable() {
    productNameController.clear();
    productId = 0;
  }
}
